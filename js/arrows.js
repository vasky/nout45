

$(document).ready( function(){
    if(!localStorage.dialogsStatus)localStorage.setItem('dialogsStatus', true);
    if(localStorage.dialogsStatus == "true"){
        $(".dialogs").removeClass("hidden");
        $(".divCircle").addClass("hidden");
    }else{
        $(".dialogs").addClass("hidden");
        $(".divCircle").removeClass("hidden");
    }

    console.log(localStorage.dialogsStatus);
});

$(".arrowBack").on("click", function(){

    localStorage.dialogsStatus = false;
    
    $(".dialogs").toggleClass("hidden");
    $(".divCircle").toggleClass("hidden");

});

$(".divCircle").on("click", function(){
    localStorage.dialogsStatus = true;

    $(".dialogs").toggleClass("hidden");
    $(".divCircle").toggleClass("hidden");

});


;