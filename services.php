<h4>Услуги</h4>
<hr>
<div class="descriptionservices">
<div class="row description">
Сервисный центр "Ноут-45" создан для предоставления населению и организациям качественных услуг по ремонту компьютерной техники.

Используя современное оборудование и многолетний опыт работы, специалисты сервисного центра выполнят работы качественно и в срок.

Однако, в отдельных случаях, сроки ремонта могут быть увеличены из-за необходимости заказа определенных запчастей и расходных материалов.
</div>

<div class="row tables">
<div class="row col-lg-12 firsttableservices">
<table class="table col-lg-12 table-striped table-dark">
  <thead>
    <tr class="col">
      <th scope="col-lg-12">
            Ремонт ЖК мониторов
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Услуга</td>
      <td>Цена</td>
    </tr>
    <tr>
      <td>Диагностика (при условии последующего ремонта у нас)</td>
      <td>Бесплатно</td>
    </tr>
    <tr>
      <td>Диагностика без ремонта</td>
      <td>	
        600 руб
      </td>
    </tr>
    <tr>
      <td>Ремонт монитора</td>
      <td>	
        1200 руб
      </td>
    </tr>
  </tbody>
</table>

<table class="table col-lg-12 table-striped table-dark">
  <thead>
    <tr class="col">
      <th scope="col-lg-12">
            Ремонт системных блоков (без стоимости запчастей)
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Услуга</td>
      <td>Цена</td>
    </tr>
    <tr>
      <td>Диагностика (при условии последующего ремонта у нас)</td>
      <td>Бесплатно</td>
    </tr>
    <tr>
      <td>Диагностика без ремонта</td>
      <td>	
        400 руб
      </td>
    </tr>
    <tr>
      <td>Чистка стационарного компьютера с полной разборкой, заменой термопасты и смазыванием вентиляторов</td>
      <td>	
        600 руб
      </td>
    </tr>
    <tr>
      <td>Замена материнской платы</td>
      <td>	
        600 руб
      </td>
    </tr>
    <tr>
      <td>Снятие / установка материнской платы в корпус компьютера</td>
      <td>	
        600 руб
      </td>
    </tr>
    <tr>
      <td>Замена видеокарты</td>
      <td>	
        300 руб
      </td>
    </tr>
    <tr>
      <td>Замена блока питания</td>
      <td>	
        200 руб
      </td>
    </tr>
    <tr>
      <td>Замена жесткого диска</td>
      <td>	
        300 руб
      </td>
    </tr>
    <tr>
      <td>Замена сетевой платы LAN</td>
      <td>	
      300 руб + стоимость платы (500 руб  - 1500 руб)
      </td>
    </tr>
    <tr>
      <td>Ремонт материнской платы</td>
      <td>	
      500 руб – 1200 руб в зависимости от сложности
      </td>
    </tr>
    <tr>
      <td>Ремонт видеокарты</td>
      <td>	
      300 руб – 1000 руб в зависимости от сложности
      </td>
    </tr>
    <tr>
      <td>Ремонт блока питания</td>
      <td>	
        500 руб
      </td>
    </tr>
    <tr>
      <td>Ремонт жесткого диска</td>
      <td>	
      200 руб – 3000 руб в зависимости от сложности
      </td>
    </tr>
  </tbody>
</table>

<table class="table col-lg-12 table-striped table-dark">
  <thead>
    <tr class="col">
      <th scope="col-lg-12">
      Ремонт ноутбуков
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Услуга</td>
      <td>Цена</td>
    </tr>
    <tr>
      <td>Диагностика (при условии последующего ремонта у нас)</td>
      <td>Бесплатно</td>
    </tr>
    <tr>
      <td>Диагностика без ремонта</td>
      <td>	
        400 руб
      </td>
    </tr>
    <tr>
      <td>Ремонт внешнего блока питания</td>
      <td>	
        600 руб
      </td>
    </tr>
    <tr>
      <td>Замена LCD-матрицы (экрана ноутбука)</td>
      <td>	
      стоимость матрицы ( от 2500 руб до 4500 руб) + установка 700 руб 
      </td>
    </tr>
    <tr>
      <td>Восстановление подсветки дисплеев</td>
      <td>	
      от 1500 руб + стоимость инвертора или ламп
      </td>
    </tr>
    <tr>
      <td>Ремонт кнопок ноутбука (механическая часть)</td>
      <td>	
      от 100 руб за 1 кнопку
      </td>
    </tr>
    <tr>
      <td>Замена клавиатуры ноутбука</td>
      <td>	
      300 руб + стоимость клавиатуры (1500 руб  - 2500 руб)
      </td>
    </tr>
    <tr>
      <td>Ремонт материнской платы ноутбука</td>
      <td>	
      от 2000 руб без замены BGA-микросхем

с заменой BGA-микросхем от 3000 руб + стоимость чипа (350 руб – 1900 руб)
      </td>
    </tr>
    <tr>
      <td>Техническое обслуживание ноутбука) (чистка от пыли, замена термопасты)</td>
      <td>	
      700 руб, если полная разборка не требуется<br>
      1000 руб, если требуется полная разборка
      </td>
    </tr>
    <tr>
      <td>Замена вентилятора системы охлаждения</td>
      <td>	
      1000 руб + стоимость вентилятора(800 руб  - 1500 руб)
      </td>
    </tr>
    <tr>
      <td>Замена разъема зарядки ноутбука</td>
      <td>	
        от 1400 руб
      </td>
    </tr>
    <tr>
      <td>Замена разъема USB</td>
      <td>	
        1000 руб
      </td>
    </tr>
    <tr>
      <td>Замена DVD-дисковода</td>
      <td>	
      300руб+стоимость дисковода (1000р)
      </td>
    </tr>
    <tr>
      <td>Замена жесткого диска</td>
      <td>	
      350 руб, если полная разборка не требуется<br>
      500 руб, если требуется полная разборка
      </td>
    </tr>
    <tr>
      <td>Восстановление информации</td>
      <td>	
      программное, без использования спец.оборудования, восстановление случайно удалённых файлов и т.п. 2000руб
      </td>
    </tr>
    <tr>
      <td>Модернизация ноутбука</td>
      <td>	
      от 500 руб работа + стоимость устанавливаемых по согласованию с заказчиком комплектующих
      </td>
    </tr>
    <tr>
      <td>Удаление вирусов</td>
      <td>	
       от 600 руб
      </td>
    </tr>
    <tr>
      <td>Установка драйверов</td>
      <td>	
       от 400 руб
      </td>
    </tr>
    <tr>
      <td>Настройка программ операционной системы Windows (обязательно при наличии лицензии)</td>
      <td>	
        от 400 руб
      </td>
    </tr>
  </tbody>
</table>

</div>
</div>
</div>