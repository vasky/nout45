<?php  
if(isset($_GET["newsId"])){
    include( 'db.php' );
    $link = mysqli_connect($host, $user, $password, $database) 
    or die("Ошибка " . mysqli_error($link));
    $query= "SELECT * FROM `_news` WHERE `id` = " . $_GET['newsId'] . " ";

    $result = mysqli_query($link , $query);


    if(mysqli_num_rows($result)>0){
        $post =  mysqli_fetch_all($result);  
        

        ?>

            <div class="newspost container">

            <div class="row upPart ">
                <div class="leftPart col-lg-4">
                    <div class="newsPhoto newsPhotoBlock m-data">
                        <img src="<?= $post[0][4] ?>" alt="">
                    </div>
                </div>
                <div class="rightPart col-lg-8">
                    <div class="titleNewsPost row m-blog-text">
                    <?= $post[0][1]; ?>
                    </div>
                    <hr>
                    <div class="row datePost calendarPost">
                        <div class="iconpostcalendar">
                                <i class="fa fa-calendar fa-6" aria-hidden="true"></i>
                            </div>    
                            <div class="datapost">
                             <?= $post[0][5] ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row downPart">
                <div class="postText">
                <?= $post[0][2] ?>
                
                </div>
            </div>

                <div class="b-blog-return d-flex justify-content-center bottom-link">
                    <div class="buttonBack">
                        <i class="fa fa-arrow-left" ></i>
                        <a href="http://localhost/nout45/index.php?chapter=blog" >Вернуться</a>
                    </div>
                </div>  
            </div>

        <?php

    }
}
?>

