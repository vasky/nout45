<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="favicon.png" type="image/png">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <title>Сайт компании Ноут-45 (мастерская по ремонту комп.техники, магазин запчастей - новых и с разбора)</title>
</head>
<body>
    <?php 
    //include('header2.php');
    
    ?>
    <div class="dialogs hidden">
    <div class="container d-flex row pull-right align-items-center">
        <div class="textMotivating">
            <h3>Напишите нам в мессенджер</h3>
        </div>
        <ul class="dialogs_items col-lg-6">
            <li class="dialogs_item vkicon">
                <a href="https://vk.me/notebook45" target="_blank">
                    <i class="fa fa-vk" aria-hidden="true"></i>
                </a> 
            </li>
            <li class="dialogs_item okicon">
                <a href="https://ok.ru/nout45/post" target="_blank">
                    <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                </a>
            </li>
            <li class="dialogs_item youicon">
                <a href="https://www.youtube.com/channel/UCDueI6TdDnsyLrC738d6Gzw">
                    <i class="fa fa-youtube" aria-hidden="true"></i>
                </a> 
            </li>
            <li class="dialogs_item twittericon">
                <a href="">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                </a> 
            </li>
            <li class="dialogs_item vibericon">
                <a href="viber://chat?79292269186">
                    <div class="viberIcon"></div>
                </a>
            </li>
            <li class="dialogs_item whaticon">
                <a href="https://wa.me/79292269186" target="_blank">
                    <i class="fa fa-whatsapp" aria-hidden="true"></i>
                </a>
            </li>
        </ul>
            <div class="arrowBack">
            <img src="res/img/стрелочка.png"  alt="">
            </div> 
    </div>
    </div>
    <div class="divCircle hidden">
    <span class="circle">
        <img src="res/img/стрелочка.png"  alt="">
    </span>
    </div>

    
<!-- <nav class="top-menu d-flex justify-content-center">
  <ul class="menu-main">
    <li class=""><a href="?chapter=blog">Новости</a></li>
    <li class=""><a href="?chapter=services">Услуги</a></li>
    <li class=""><a href="?chapter=contacts">Контакты</a></li>
    <li class=""><a href="?chapter=feedback">Отзывы</a></li>
    <li class=""><a href="?chapter=orderCategory">Товары</a></li>
    <li class=""><a href="forum/">Форум</a></li>  
    <li class=""><a href="?chapter=order">Статус заказа</a></li>  
    <li class=""><a href="?chapter=cart">Корзина</a></li>   
    <?php 
    if(!isset($_SESSION['user'])){
    ?>
    <script src="//ulogin.ru/js/ulogin.js"></script>
    <div id="uLogin" data-ulogin="display=panel;theme=classic;fields=first_name,last_name;providers=vkontakte,odnoklassniki,mailru,facebook;hidden=other;redirect_uri=http%3A%2F%2Flocalhost%2Fnout45%2Findex.php;mobilebuttons=0;"></div>

    <?php }else
    {?>
        <a href='http://localhost/nout45/exit.php' ><button >Выйти</button></a>
        <?php
    }?>
       



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</nav>
         -->
         <div class="blockTestLine container">
<div class="mainMenu container row d-flex">
<div class="logoMainMenu col-lg-1">
    <img src="res/img/logo45.png" alt="">
</div>
<div class="itemsMainMenu d-flex justify-content-center pull-right">
    <ul class="mainMenuUl d-flex align-items-center">
        
        <li class="itemMainMenu"><a href="?chapter=blog">Новости</a></li>
        <li class="itemMainMenu"><a href="?chapter=services">Услуги</a></li>
        <li class="itemMainMenu"><a href="?chapter=contacts">Контакты</a></li>
        <li class="itemMainMenu"><a href="?chapter=feedback">Отзывы</a></li>
        <li class="itemMainMenu"><a href="?chapter=orderCategory">Товары</a></li>
        <li class="itemMainMenu"><a href="forum/">Форум</a></li>  
        <li class="itemMainMenu"><a href="?chapter=order">Статус заказа</a></li>  
        <li class="itemMainMenu"><a href="?chapter=cart">Корзина</a></li> 
    </ul>
</div>
</div>

    <div class="field container d-flex">
    <div class="before">
        <div class=""></div>
        <div class="circlesBefore">
            <div class="figura1"></div>

        </div>
    </div>
    <div class="menu col-lg-2"> боковое  меню слева</div>
    <div class="content col-lg-10">

    

   